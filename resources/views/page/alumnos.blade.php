@extends('dashboard')

@section('contenido')
    <h1>Alumnos </h1>

    <table class="table table-striped">
  ...   <thead>
            <th>No</th>
            <th>apellido paterno</th>
            <th>apellido materno</th>
            <th>semestre</th>
            <th>grupo</th>
        </thead>
        <tbody>
        @foreach ($alumnos as $alumno)
        <tr>
            <td>{{$alumno->id}}</td>
            <td>{{$alumno->nombre}}</td>
            <td>{{$alumno->apellido_paterno}}</td>
            <td>{{$alumno->apellido_materno}}</td>
            <td>{{$alumno->semestre}}</td>
            <td>{{$alumno->grupo}}</td>
        </tr>   
@endforeach
</tbody>
 </table>

   
@endsection