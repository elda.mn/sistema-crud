<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            'nombre'=>'elda',
            'apellido_paterno' => 'moreno',
            'apellido_materno' => 'nuñez',
            'semestre' =>  1,
            'grupo' => 'A',
        ]);
        //
    }
}
